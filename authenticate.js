const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/user');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const jwt = require('jsonwebtoken');

const config = require('./config');

const ops = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.secretKey,
};
module.exports = {
  getToken: (user) => {
    return jwt.sign(user, config.secretKey, { expiresIn: 3600 });
  },
  jwtPassport: passport.use(new JwtStrategy(ops,
    (jwt_payload, done) => {
      console.log('JWT payload ', jwt_payload);
      User.findOne({
        _id: jwt_payload._id,
      }, (err, user) => {
        if (err) {
          return done(err, false);
        } else if (user) {
          return done(null, user);
        }
        return done(null, false);
      });
    })),
  verifyUser: passport.authenticate('jwt', { session: false }),
  verifyAdmin: (req, res, next) => {
    if (req.user.admin) {
      return next(req, res, null)
    } else {
      const err = new Error('You are not admin user!');
      err.status = 403;
      return next(err);
    }
  },
  googlePassoport: passport.use(new GoogleStrategy({
    clientID: config.google.clientId,
    clientSecret: config.google.clientSecret,
    callbackURL: config.google.callbackURL,
  }), (accessToken, refreshToken, profile, cb) => {
    User.findOne({
      googleId: profile.id,
    }, (err, user) => {
      if (err) {
        return cb(err, false);
      }
      if (!user) {
        return cb(null, user);
      } else {
        console.log('===> ', profile);
        user = new User({
          username: profile.displayName,
        });
        user.googleId = profile.id;
        user.firstname = profile.name.givenName;
        user.lastname = profile.name.familyName;
        user.save((err, user) => {
          if (err) {
            return cb(err, false);
          } else {
            return (null, user);
          }
        });
      }
    })
  }),
}
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());