const express = require('express');
const cors = require('cors');
const app = express();

const whitelist = ['http://localhost:3000', 'https://localhost:3443'];

const corsOptionsDelegate = (req, callback) => {
  let corsOptions = {
    origin: false,
  };
  const origin = req.header('origin');
  console.log(origin);
  if (!!whitelist.find(v => v === origin)) {
    corsOptions = {
      origin: true,
    };
  }
  return callback(null, corsOptions);
}

exports.cors = cors();
exports.corsWithOptions = cors(corsOptionsDelegate);