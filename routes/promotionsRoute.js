const express = require("express");
const bodyParser = require("body-parser");
const Promotions = require('../models/promotions');
const authenticate = require('../authenticate');

const promotionsRoute = express.Router();
promotionsRoute.use(bodyParser.json());

promotionsRoute.route("/")
  .get((req, res, next) => {
    Promotions.find({})
      .then((promotions) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotions);
      }, err => next(err))
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    Promotions.create(req.body)
      .then((promotion) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
      }, err => next(err))
      .catch(err => next(err))
  })
  .put(authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
    res.statusCode = 403;
    res.end("PUT operation not supported on /promotions");
  })
  .delete(authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
    Promotions.remove({})
      .then((res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'applicaiton/json');
        res.json(res);
      }, err => next(err))
      .catch(err => next(err))
  });

promotionsRoute
  .route("/:promotionId")
  .get((req, res, next) => {
    Promotions.findById(req.params.promotionId)
      .then((promotion) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
      }, err => next(err))
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
    res.statusCode = 403;
    res.end("POST operation not supported on /promotions/" + req.params.promotionId);
  })
  .put(authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
    Promotions.findByIdAndUpdate(req.params.promotionId, {
      $set: req.body,
    }, { new: true })
      .then((promotion) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
      }, err => next(err))
      .catch(err => next(err))
  })
  .delete(authenticate.verifyUser, authenticate.verifyAdmin,  (req, res, next) => {
    Promotions.findByIdAndRemove(req.params.promotionId)
      .then(res => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(res);
      }, err => next(err))
      .catch(err => next(err))
  });

module.exports = promotionsRoute;

