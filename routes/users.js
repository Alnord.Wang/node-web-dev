var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const User = require('../models/user');
const passport = require('passport');
const authenticate = require('../authenticate');

router.use(bodyParser.json());
/* GET users listing. */
router.get('/', authenticate.verifyUser, function(req, res, next) {
  if (req.user.admin) {
    User.find({}).then(users => {
      res.send(users);
    });  
  } else {
    res.send('respond with a resource');
  }
});
router.post('/signup', (req, res, next) => {
  User.register(new User({
    username: req.body.username,
  }), req.body.password, (err, user) => {
    if (err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'appliction/json');
      res.json({err: err});
    } else {
      if (req.body.firstname) {
        user.firstname = req.body.firstname;
      }
      if (req.body.lastname) {
        user.lastname = req.body.lastname;
      }
      user.admin = user.username === 'admin';
      user.save((err, user) => {
        if (err) {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'appliction/json');
          res.json({err: err});
          return;
        }
        passport.authenticate('local')(req,res, () => {
          res.statusCode = 200;
          res.setHeader('Content-Type', 'application/json');
          res.json({
            success: true,
            status: 'Registration Successful!'
          });
        });
      })
    }
  })
});
router.post('/login', passport.authenticate('local'), (req, res, next) => {
  const token = authenticate.getToken({
    _id: req.user._id,
  });
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({
    success: true,
    token: token,
    status: 'You are successfully logged in!'
  });
});
router.get('/logout', (req, res) => {
  if (req.session) {
    req.session.destroy();
    res.clearCookie('session-id');
    res.redirect('/');
  } else {
    const err = new Error('You are not logged in!');
    err.status = 403;
    next(err);
  }
})
router.get('/auth/google/token', passport.authenticate('google', { scope: 'profile' }), (req, res) => {
  if (req.user) {
    const token = authenticate.getToken({
      _id: req.user._id,
    });
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({
      sucess: true,
      token,
      status: 'You are sucesfully logged in!'
    });
  }
})
module.exports = router;
