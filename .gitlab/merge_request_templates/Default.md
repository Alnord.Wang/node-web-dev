# Before you open a MR

Before you open a MR, please make it easy to review. 
## Tips
  - Add inline comments or add comment in code to help reviewer to understand code.
- Separate the large and complex MR into multi MRs for reviewing.
## Reference Link:
  - https://coding-time.co/code-review/

# Description

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

Fixes # (issue)

## Bug Description (required if you try to fix bug)
Clearly and concisely describe the problem
## Root Cause (required if you try to fix bug)
Briefly describe the root cause and analysis of the problem

## Type of change

Please delete options that are not relevant.

- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to not work as expected)
- [ ] This change requires a documentation update

## Test plan (required)
> The test plan is there to prove the code works. (https://coding-time.co/what-is-a-test-plan/)

- [ ] Test A
- [ ] Test B

# Checklist:
- [ ] I have linked the JIRA ticket in my MR title
- [ ] I have handle the error and excepation in my code
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have made corresponding changes to the documentation
- [ ] My changes generate no new warnings
- [ ] I have added tests that prove my fix is effective or that my feature works
- [ ] New and existing unit tests pass locally with my changes
- [ ] Any dependent changes have been merged and published in downstream modules